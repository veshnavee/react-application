import React,{Component} from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, Breadcrumb, BreadcrumbItem,
    Button, Modal , ModalHeader, ModalBody, Label,Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import {Loading} from './LoadingComponent';
import { baseUrl } from  '../shared/baseUrl';

import { FadeTransform , Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };

        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleSubmit(values) {
        console.log("Current State is: " + JSON.stringify(values));
        alert("Current State is: " + JSON.stringify(values));
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);

    }

     render(){
         return(
            <React.Fragment>
            <Button outline color="primary" onClick={this.toggleModal}> 
                <span className="fa fa-pencil fa-lg"> Submit Comment</span>
            </Button>
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>
                    Submit Comment
                </ModalHeader>
                <ModalBody className="ml-3 mr-3">
                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                        <Row className="form-group" ml={2}>
                            <Label htmlFor="rating">Rating</Label>
                            <Control.select model=".rating" name="rating" 
                            className="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </Control.select>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="author">Your Name</Label>
                            <Control.text model=".author" id="author" name="author" 
                            placeholder="Your Name" 
                            className="form-control"
                            validators={{
                                required, 
                                minLength: minLength(3), 
                                maxLength: maxLength(15)
                            }}
                            />
                            <Errors 
                                className="text-danger"
                                model=".author"
                                show="touched"
                                messages={{
                                    required: 'Required',
                                    minLength: 'Must be greater than 2 characters',
                                    maxLength: 'Must be 15 characters or less'
                                }}
                            />
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="comment">Comment</Label>
                                <Control.textarea model=".comment" id="comment" name="comment" rows="6" 
                                className="form-control"/>
                        </Row>
                        <Row className="form-group">
                                <Button type="submit" color="primary">Submit</Button>
                        </Row>
                    </LocalForm>
                </ModalBody>
            </Modal>
            </React.Fragment>
         );
     }

}

function DishDetail (props) {

    console.log("Dish Details render is Invoked");
    if(props.isLoading){
        return(
            <div className="container">
                <div className="row">
                    <Loading/>
                </div>
            </div>
        );
    }

    else if(props.errmess){
        return(
            <div className="container">
                <div className="row">
                    <h4>{props.errmess}</h4>
                </div>
            </div>
        );

    }

    else if (props.selectedDish != null) {
        console.log(props.comments);
        console.log("postComment : "+ typeof props.postComment);
        const comments = props.comments.map((x) => {
            return (
                <div key={x.id}>
                    <div><h6>{x.comment}</h6></div>
                    <div>--{x.author},{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(x.date)))}</div>
                    <br />
                </div>
            );
        });
        return (
            <div className="Container">
                <div className="row m-1">
                    <Breadcrumb>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.selectedDish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.selectedDish.name}</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row m-1">
                    <div className=" col-12 col-md-5">
                    <FadeTransform in transformProps= {{
                exitTransform: 'scale(0.5) transletY(-50%)'
            }}>
                        <Card>
                            <CardImg width="100%" src={baseUrl + props.selectedDish.image} alt={props.selectedDish.name} />
                            <CardBody>
                                <CardTitle>{props.selectedDish.name}</CardTitle>
                                <CardText>{props.selectedDish.description}</CardText>
                            </CardBody>
                       </Card>
                       </FadeTransform>
                    </div>
                    <div className="col-12 col-md-5">
                        {comments}
                        <CommentForm postComment={props.postComment}
                                     dishId={props.selectedDish.id}/>
                    </div>
                </div>
            </div>
        );
    }
    else {
        return (<div></div>);
    }
}

export default DishDetail;