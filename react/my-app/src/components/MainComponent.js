import React, { Component } from 'react';
import Menu from './MenuComponent';
import DishDetail from './DishDetailComponent';
import About from './AboutComponent';
// import { DISHES } from '../shared/dishes';
// import { COMMENTS } from '../shared/comments';
// import { LEADERS } from '../shared/leaders';
// import { PROMOTIONS } from '../shared/promotions';



import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Contact from './contactComponent';
import { connect } from 'react-redux';
import { postComment, postFeedback , fetchDishes, fetchComments, fetchPromos , fetchLeaders } from '../redux/ActionCreators';
import { actions } from 'react-redux-form';

import {TransitionGroup,CSSTransition} from 'react-transition-group'

const mapStateToProps = state => {
  return{
    dishes : state.dishes,
    leaders : state.leaders,
    comments : state.comments,
    promotions : state.promotions
  }
};

const mapDispatchToProps = dispatch => ({
  postComment:(dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
  fetchDishes : () => {dispatch(fetchDishes())},
  fetchComments : () => {dispatch(fetchComments())},
  fetchPromos : () => {dispatch(fetchPromos())},
  fetchLeaders : () => {dispatch(fetchLeaders())},
  resetFeedbackForm: () => { dispatch(actions.reset('feedback'))},
  postFeedback : () => {dispatch(postFeedback())}
});

class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
        // dishes: DISHES,
        // comments: COMMENTS,
        // leaders: LEADERS,
        // promotions: PROMOTIONS
        // selectedDish: null
    };
  }

  

  // onDishSelect(dishId) {
  //   this.setState({ selectedDish: dishId});
  // }

  componentDidMount(){
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();
  }

  
  render() {
  const HomePage = () => {
      return(
        <Home 
        dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
        dishesLoading={this.props.dishes.isLoading}
        dishesErrMess={this.props.dishes.errmess}
        promotion={this.props.promotions.promotions.filter((promo) => promo.featured)[0]}
        promosLoading={this.props.promotions.isLoading}
        promosErrMess={this.props.promotions.errmess}
        leader={this.props.leaders.leaders.filter((leader) => leader.featured)[0]}
        leadersLoading={this.props.leaders.isLoading}
        leadersErrMess={this.props.leaders.errmess}
    />
      );
   }
   const DishWithId = ({match}) => {
    return(
      <DishDetail selectedDish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]}
      dishesloading = {this.props.dishes.isLoading}
      dishesErrMess = {this.props.errmess}
      comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))}
      postComment={this.props.postComment}  
      commentsErrMess={this.props.comments.errmess}
 
      />
    );
  }

   const AboutUs = () =>{
     return(
       <About leaders= {this.props.leaders.leaders} />
     );
   }

    return (
      <div>
        <Header/>
        <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
          <Switch>
            <Route path='/home' component={HomePage} />
            <Route path='/aboutus' component={AboutUs} />
            <Route exact path='/menu' component={() => <Menu dishes={this.props.dishes} />} />
            <Route exact path='/contactus' component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm}
              postFeedback={this.props.postFeedback} />} />
            <Route path='/menu/:dishId' component={DishWithId} />
            <Redirect to="/home" />
          </Switch>
          </CSSTransition>
        </TransitionGroup>
        {/* <div className="container">
        <div className="row">
             <Menu dishes={this.state.dishes} onClick={(dishId) => this.onDishSelect(dishId)} />
           </div>
        <div className="row">
             <DishDetail selectedDish={this.state.dishes.filter((dish) => dish.id === this.state.selectedDish)[0]} />
           </div>
        </div> */}
        <Footer/>
        </div>
    );
  }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));